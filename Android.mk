LOCAL_PATH:= $(call my-dir)

ifneq ($(filter surya,$(TARGET_DEVICE)),)

# artifacts from caf build
$(call add-radio-file,cafvendor.img)
$(call add-radio-file,dtbo.img)

# vendor-specific vendor operation list - autogenerated
$(call add-radio-file,vendor_op_list)

# firmware
$(call add-radio-file,fw/abl.elf)
$(call add-radio-file,fw/aop.mbn)
$(call add-radio-file,fw/cmnlib.mbn)
$(call add-radio-file,fw/cmnlib64.mbn)
$(call add-radio-file,fw/devcfg.mbn)
$(call add-radio-file,fw/ffu.img)
$(call add-radio-file,fw/hyp.mbn)
$(call add-radio-file,fw/imagefv.elf)
$(call add-radio-file,fw/km4.mbn)
$(call add-radio-file,fw/qupv3fw.elf)
$(call add-radio-file,fw/storsec.mbn)
$(call add-radio-file,fw/tz.mbn)
$(call add-radio-file,fw/uefi_sec.mbn)
$(call add-radio-file,fw/xbl.elf)
$(call add-radio-file,fw/xbl_config.elf)

endif
